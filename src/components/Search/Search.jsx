

import styles from './Search.module.css'
import TextField from '@mui/material/TextField';
export default function Search() {
    return (
        <div >
            <TextField id="filled-basic" label="Search" variant="filled" className={styles.SearchMain} />

        </div>
    );
}
