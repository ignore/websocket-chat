import {
    BrowserRouter as Router,
     NavLink
} from "react-router-dom";
import React from "react";

import styles from './Appbar.module.css'
import profilepic from '../../assets/images/profile.png';

// icons 
import EmailIcon from '@mui/icons-material/Email';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import HomeIcon from '@mui/icons-material/Home';
import SettingsIcon from '@mui/icons-material/Settings';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';


export default function Appbar() {


    const Linkstyle = ({ isActive }) => ({
        width: '100 %',
        padding: '10px 0px',
        margin: '10px 0px',
        borderRight: isActive ? '3px solid #F3B559' : '',
        color: isActive ? '#fff' : '#fff',
        background: isActive ? 'rgba(97, 45, 209, 0.9)' : 'transparent',
        fontSize: '25px'
    })



    return (
        <div className={styles.appbar_main}>

            <Router>
                <div className={styles.appbar_wrapper}>


                    <img className={styles.profileImage} src={profilepic} alt="profile avatar" />
                    <NavLink to="/" style={Linkstyle}>
                        <span >
                            <HomeIcon className={styles.IconStyle} />
                        </span>

                    </NavLink>

                    <NavLink to="/message" style={Linkstyle}  >
                        <span >
                            <EmailIcon className={styles.IconStyle} />
                        </span>
                    </NavLink>

                    <NavLink to="/notif" style={Linkstyle}><span >
                        <NotificationsNoneIcon className={styles.IconStyle} />
                    </span></NavLink>
                    <NavLink to="/setting" style={Linkstyle} ><span >
                        <SettingsIcon className={styles.IconStyle} />
                    </span></NavLink>
                    <NavLink to="/logout" style={Linkstyle} ><span >
                        <ExitToAppIcon className={styles.IconStyle} />
                    </span></NavLink>
                </div>
                {/* <Switch>
                        <Route path="/about">
                            <Notif />
                        </Route>
                        <Route path="/users">
                            <Setting />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch> */}
            </Router >
        </div >
    );
}
