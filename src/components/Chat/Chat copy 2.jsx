import { Fragment, useEffect, useRef, useState } from "react";

import styles from './Chat.module.css'
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import { ChatMessageDto } from "../../model/ChatMessageDto";
import { Container, FormControl, Paper } from "@mui/material";
import { Box } from "@mui/system";
import SendIcon from '@mui/icons-material/Send';

import IconButton from '@mui/material/IconButton';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import KeyboardVoiceIcon from '@mui/icons-material/KeyboardVoice';

// icons 

import MoreVertIcon from '@mui/icons-material/MoreVert';
import VideocamIcon from '@mui/icons-material/Videocam';
import CallIcon from '@mui/icons-material/Call';

export default function Chat() {
    const ENTER_KEY_CODE = 13;

    const scrollBottomRef = useRef(null);
    const webSocket = useRef(null);
    const [chatMessages, setChatMessages] = useState([]);
    const [user, setUser] = useState('');
    const [id, setId] = useState(1);
    const [message, setMessage] = useState('');
    const [servers, setservers] = useState('');
    const [responseChatData, setResponseChatData] = useState()

    useEffect(() => {


        console.log('Opening WebSocket');

        webSocket.current = new WebSocket('ws://172.20.1.195:8000/WebSocket');
        const openWebSocket = () => {
            webSocket.current.onopen = (event) => {
                console.log('Open:', event);
            }
            webSocket.current.onclose = (event) => {
                console.log('Close:', event);
            }
        }
        openWebSocket();
        return () => {
            console.log('Closing WebSocket');
            webSocket.current.close();
        }
    }, []);

    useEffect(() => {
        webSocket.current.onmessage = (event) => {
            const chatMessageDto = JSON.parse(event.data);

            // setResponseChatData(chatMessageDto)
            // setChatMessages([...chatMessages, {

            //     message: chatMessageDto.message,
            //     servers: true

            // }]);
            console.log('Message:', ...chatMessages);

            if (scrollBottomRef.current) {
                scrollBottomRef.current.scrollIntoView({ behavior: 'smooth' });
            }
        }
    }, [chatMessages]);
    const handleUserChange = (event) => {
        setUser(event.target.value);
    }

    const handleMessageChange = (event) => {
        // console.log(event.target.value)
        
            setMessage(event.target.value);
            // sendMessage();
      
        
    }

    const handleEnterKey = (event) => {
        // console.log(event)
        
    }
    const sendMessage = () => {
        if (message) {

            webSocket.current.send(
                JSON.stringify(new ChatMessageDto(user, message, servers))
            );
            setMessage('');
        }
    };

    const listChatMessages = chatMessages.map((chatMessageDto, index) =>

        <ListItem key={index} className={styles.MessageWrapper}>

            <ListItemText primary={` ${chatMessageDto.message}`} className={chatMessageDto.user == user ? styles.response : styles.request} />
        </ListItem>

    );



    return (
        <div className={styles.Chat}>
            <Grid container spacing={2} className={styles.chatWrapper} >
                <Grid item xs={8} md={8}>
                    <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                        <ListItem alignItems="flex-start">
                            <ListItemAvatar>
                                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                            </ListItemAvatar>
                            <ListItemText
                                primary="Anil"
                                secondary={
                                    <Fragment>
                                        <Typography
                                            sx={{ display: 'inline' }}
                                            component="span"
                                            variant="body2"
                                            color="text.primary"
                                        >
                                            Online
                                        </Typography>
                                        {" - Last seen, 2.02pm"}
                                    </Fragment>
                                }
                            />
                        </ListItem>
                    </List>
                </Grid>
                <Grid item xs={4} md={4} className={styles.Iconwrapper}>
                    <IconButton >
                        <CallIcon className={styles.iconStyle} />
                    </IconButton>
                    <IconButton >
                        <VideocamIcon className={styles.iconStyle} />
                    </IconButton>
                    <IconButton >
                        <MoreVertIcon className={styles.iconStyle} />
                    </IconButton>
                </Grid>
            </Grid>
            <Divider />
            <Fragment>


                <Box p={3}>

                    <Grid container spacing={4} alignItems="center" className={styles.ChatInput}>
                        <Grid id="chat-window" xs={12} item>
                            <List id="chat-window-messages">
                                {listChatMessages}

                                <ListItem ref={scrollBottomRef}></ListItem>
                            </List>
                        </Grid>

                        <Grid xs={11} item>
                            <FormControl fullWidth>
                                <TextField onChange={handleMessageChange} onKeyDown={handleEnterKey}
                                    value={message}
                                    label="Type your message..."
                                    variant="outlined" />
                            </FormControl>
                        </Grid>
                        <Grid xs={1} item>
                                <IconButton onClick={sendMessage}
                                    aria-label="send"
                                    color="primary">
                                    <SendIcon />
                                </IconButton>
                            </Grid>



                    </Grid>
                </Box>


            </Fragment>
            {/* <ul className={styles.MessageWrapper}>
                <ul className={styles.MessageWrapper}>
                    <li className={styles.response}>
                        Hey There!
                    </li>
                    <li className={styles.response}>
                        Hey There!
                    </li>
                </ul>
                <ul className={styles.MessageWrapperRight}>
                    <li className={styles.request}>
                        Hey There!
                    </li>
                    <li className={styles.request}>
                        I am fine and how are you?
                    </li>
                    <li className={styles.request}>
                        Hey There!
                    </li>
                </ul>


            </ul>
            <div className={styles.ChatInput}>
                <Grid container S>
                    <Grid item xs={11} md={10}>
                        <input type="text" className={styles.ChatTextField} placeholder="Type your message here... " />

                    </Grid>
                    <Grid item xs={1} md={1} className={styles.iconStyle}>

                        <KeyboardVoiceIcon />

                    </Grid>


                </Grid>
            </div> */}

        </div>
    );
}
