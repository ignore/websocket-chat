import './App.css';
import Grid from '@mui/material/Grid';
import Appbar from './components/Appbar/Appbar';
import ChatGroup from './components/ChatGroup/ChatGroup';
import ChatPeople from './components/ChatPeople/ChatPeople';
import Chat from './components/Chat/Chat';

function App() {
  return (
    <div className="App">
      <Grid container spacing={2}>
        <Grid item xs={12} md={1}>
          <Appbar />
        </Grid>
        <Grid item xs={12} md={4}>
          <ChatGroup />
          <ChatPeople />
        </Grid>
        <Grid item xs={12} md={7}>
          <Chat />
        </Grid>


      </Grid>
    </div>
  );
}

export default App;
